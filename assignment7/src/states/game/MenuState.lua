--[[
    GD50
    Pokemon

    Author: Colton Ogden
    cogden@cs50.harvard.edu
]]

MenuState = Class{__includes = BaseState}

function MenuState:init(oldStats, statIncreases, onExitCallback)
    self.oldStats = oldStats
    self.statIncreases = statIncreases
    self.onExit = onExitCallback or function() return end

    local width, height = 140, 100

    self.menu = Menu {
        items = self:getStatLines(),
        x = VIRTUAL_WIDTH / 2 - width,
        y = VIRTUAL_HEIGHT / 2 - height + 15,
        width = width,
        height = height,
        passive = true
    }
end

function MenuState:getStatLines()
    local statNames = {
        " HP",
        "ATK",
        "DEF",
        "SPD"
    }

    local lines = {}
    for i = 1, #statNames do
        local old = self.oldStats[i]
        local increase = self.statIncreases[i]

        table.insert(lines, {
            text = statNames[i] .. ": "
            .. tostring(old)
            .. " +" .. tostring(increase)
            .. " = " .. tostring(old + increase)
        })
    end

    return lines
end

function MenuState:update(dt)
    self.menu:update(dt)

    if love.keyboard.wasPressed('return') or love.keyboard.wasPressed('enter') then
        gSounds['blip']:stop()
        gSounds['blip']:play()

        gStateStack:pop()

        self.onExit()
    end
end

function MenuState:render()
    self.menu:render()
end