--[[
    PauseState

    Locks the game until the pause button is pressed again.
    Plays a sound and stops the music when pausing.
    Displays a slowly flashing icon during pause.
]]

PauseState = Class{__requires = BaseState}


function PauseState:init()
    self.playState = nil
    self.iconTimer = -1
end


function PauseState:enter(playState)
    -- store play state to resume it after pause
    self.playState = playState

    -- stop scrolling
    -- scrolling = false

    -- stop music
    sounds['music']:pause()

    -- play sound effect
    sounds['pause']:play()
end

function PauseState:exit()
    -- play sound effect
    sounds['unpause']:play()

    -- resume music
    sounds['music']:resume()
end

function PauseState:update(dt)
    -- flash the icon
    self.iconTimer = self.iconTimer + dt
    if self.iconTimer > 1 then
        self.iconTimer = -1
    end

    -- change state to play when pause key is pressed again
    if love.keyboard.wasPressed('p') or love.keyboard.wasPressed('lshift') or love.keyboard.wasPressed('rshift') then
        gStateMachine:change('play', self.playState)
    end
end

function PauseState:render()
    -- keep rendering what was on screen during play
    self.playState:render()

    -- render the icon
    local r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(255, 255, 255, math.abs(255 * self.iconTimer))

    love.graphics.setFont(hugeFont)
    love.graphics.printf('I I', 0, 120, VIRTUAL_WIDTH, 'center')
    love.graphics.setColor(r, g, b, a)
end