--[[
    GD50
    Breakout Remake

    -- PlayState Class --

    Author: Colton Ogden
    cogden@cs50.harvard.edu

    Represents the state of the game in which we are actively playing;
    player should control the paddle, with the ball actively bouncing between
    the bricks, walls, and the paddle. If the ball goes below the paddle, then
    the player should lose one point of health and be taken either to the Game
    Over screen if at 0 health or the Serve screen otherwise.
]]

PlayState = Class{__includes = BaseState}

--[[
    We initialize what's in our PlayState via a state table that we pass between
    states as we go from playing to serving.
]]
function PlayState:enter(params)
    self.paddle = params.paddle
    self.bricks = params.bricks
    self.health = params.health
    self.score = params.score
    self.highScores = params.highScores
    self.balls = params.balls
    self.level = params.level

    self.recoverPoints = 5000

    -- the paddle will attempt to grow every time the player
    -- scores this many points in a row.
    self.growthThreshold = params.growthThreshold
    -- this is not passed to other states since it must reset
    -- at each state change
    self.growthPoints = 0

    -- give the balls random starting velocities
    for k, ball in pairs(self.balls) do
        ball.dx = math.random(-200, 200)
        ball.dy = math.random(-50, -60)
    end

    self.powerups = {}

    -- check that at least one brick is locked
    -- to determine if we need to allow key pickups to spawn
    self.keyAllowed = false
    for k, brick in pairs(self.bricks) do
        if brick.locked then
            self.keyAllowed = true
            break
        end
    end
end

function PlayState:update(dt)
    if self.paused then
        if love.keyboard.wasPressed('space') then
            self.paused = false
            gSounds['pause']:play()
        else
            return
        end
    elseif love.keyboard.wasPressed('space') then
        self.paused = true
        gSounds['pause']:play()
        return
    end

    -- update positions based on velocity
    self.paddle:update(dt)

    local newBalls = {}
    for k, ball in pairs(self.balls) do
        ball:update(dt)

        if ball:collides(self.paddle) then
            -- raise ball above paddle in case it goes below it, then reverse dy
            ball.y = self.paddle.y - 8
            ball.dy = -ball.dy

            --
            -- tweak angle of bounce based on where it hits the paddle
            --

            -- if we hit the paddle on its left side while moving left...
            if ball.x < self.paddle.x + (self.paddle.width / 2) and self.paddle.dx < 0 then
                ball.dx = -50 + -(8 * (self.paddle.x + self.paddle.width / 2 - ball.x))
            
            -- else if we hit the paddle on its right side while moving right...
            elseif ball.x > self.paddle.x + (self.paddle.width / 2) and self.paddle.dx > 0 then
                ball.dx = 50 + (8 * math.abs(self.paddle.x + self.paddle.width / 2 - ball.x))
            end

            gSounds['paddle-hit']:play()
        end

        -- if ball goes below bounds, avoid refreshing its existence
        if ball.y >= VIRTUAL_HEIGHT then
            gSounds['hurt']:play()
        else
            -- else keep it in play
            table.insert(newBalls, ball)
        end
    end

    -- if there are no more balls in play
    if #newBalls == 0 then
        self.health = self.health - 1

        -- decrease paddle size
        self.paddle:setSize(self.paddle.size - 1)
        -- reset growth points but make threshold bigger
        self.growthPoints = 0
        self.growthThreshold:decrease()
        -- if the paddle is of the smallest size, decrease threshold again if possible,
        -- to give extra help to the player
        if self.paddle.size == 1 then
            self.growthThreshold:decrease()
        end

        if self.health == 0 then
            gStateMachine:change('game-over', {
                score = self.score,
                highScores = self.highScores
            })
        else
            gStateMachine:change('serve', {
                paddle = self.paddle,
                bricks = self.bricks,
                health = self.health,
                score = self.score,
                highScores = self.highScores,
                level = self.level,
                recoverPoints = self.recoverPoints,
                growthThreshold = self.growthThreshold,
            })
        end
    end
    self.balls = newBalls

    -- detect collision across all bricks with the ball
    for k, brick in pairs(self.bricks) do

        -- only check collision if we're in play
        if brick.inPlay then

            local collidingBall = nil
            for b, ball in pairs(self.balls) do
                if ball:collides(brick) then
                    collidingBall = ball
                    break
                end
            end

            -- only check one ball to avoid duplicate concurrent hits on the same brick
            if collidingBall then

                -- don't award points when hitting locked bricks
                if not brick.locked then
                    local brickScore = (brick.tier * 200 + brick.color * 25)
                    -- add to score
                    self.score = self.score + brickScore
                    self.growthPoints = self.growthPoints + brickScore

                    -- grow paddle size if threshold reached. Increase threshold.
                    if self.growthPoints >= self.growthThreshold:getValue() then
                        self.paddle:setSize(self.paddle.size + 1)
                        self.growthPoints = 0
                        self.growthThreshold:increase()

                        -- play a sound effect
                        gSounds['grow']:play()
                    end
                end

                -- randomise chance to get a random powerup drop
                local newPowerup = nil
                local powerupChance = math.random(100)
                if powerupChance <= 14 then
                    newPowerup = Powerup('random')
                end
                -- if no other powerup spawned and the conditions are good
                -- give a 10% chance of spawning a key
                -- provided there isn't already a key in play
                if self.keyAllowed and not newPowerup and powerupChance > 80 then
                    newPowerup = Powerup('key')
                    self.keyAllowed = false
                end

                -- initialise the new powerup if any
                if newPowerup then
                    table.insert(self.powerups, newPowerup)
                    newPowerup:spawn(collidingBall.x, collidingBall.y)
                end

                -- trigger the brick's hit function, which removes it from play
                brick:hit()

                -- if we have enough points, recover a point of health
                if self.score > self.recoverPoints then
                    -- can't go above 3 health
                    self.health = math.min(3, self.health + 1)

                    -- multiply recover points by 2
                    self.recoverPoints = math.min(100000, self.recoverPoints * 2)

                    -- play recover sound effect
                    gSounds['recover']:play()
                end

                -- go to our victory screen if there are no more bricks left
                if self:checkVictory() then
                    gSounds['victory']:play()

                    gStateMachine:change('victory', {
                        level = self.level,
                        paddle = self.paddle,
                        health = self.health,
                        score = self.score,
                        highScores = self.highScores,
                        balls = self.balls,
                        recoverPoints = self.recoverPoints,
                        growthThreshold = self.growthThreshold
                    })
                end

                -- collision code for bricks
                --
                -- we check to see if the opposite side of our velocity is outside of the brick;
                -- if it is, we trigger a collision on that side. else we're within the X + width of
                -- the brick and should check to see if the top or bottom edge is outside of the brick,
                -- colliding on the top or bottom accordingly 
                --

                -- left edge; only check if we're moving right, and offset the check by a couple of pixels
                -- so that flush corner hits register as Y flips, not X flips
                if collidingBall.x + 2 < brick.x and collidingBall.dx > 0 then
                    
                    -- flip x velocity and reset position outside of brick
                    collidingBall.dx = -collidingBall.dx
                    collidingBall.x = brick.x - 8
                
                -- right edge; only check if we're moving left, , and offset the check by a couple of pixels
                -- so that flush corner hits register as Y flips, not X flips
                elseif collidingBall.x + 6 > brick.x + brick.width and collidingBall.dx < 0 then
                    
                    -- flip x velocity and reset position outside of brick
                    collidingBall.dx = -collidingBall.dx
                    collidingBall.x = brick.x + 32
                
                -- top edge if no X collisions, always check
                elseif collidingBall.y < brick.y then
                    
                    -- flip y velocity and reset position outside of brick
                    collidingBall.dy = -collidingBall.dy
                    collidingBall.y = brick.y - 8
                
                -- bottom edge if no X collisions or top collision, last possibility
                else
                    
                    -- flip y velocity and reset position outside of brick
                    collidingBall.dy = -collidingBall.dy
                    collidingBall.y = brick.y + 16
                end

                -- slightly scale the y velocity to speed up the game, capping at +- 150
                if math.abs(collidingBall.dy) < 150 then
                    collidingBall.dy = collidingBall.dy * 1.02
                end

                -- only allow colliding with one brick, for corners
                break
            end
        end
    end

    -- check if each powerup is still in play and
    -- if it collides with the paddle
    -- remove from play if below edge or is activated
    local currentPowerups = {}
    for k, powerup in pairs(self.powerups) do
        powerup:update(dt)

        local powerupCollected = powerup:collides(self.paddle)

        if powerup.inPlay
        and powerupCollected then
            powerup.inPlay = false

            -- some points for any powerup
            self.score = self.score + 200
            gSounds['powerup']:play()

            -- activate an effect relative to the powerup type
            local powerType = powerup.powerupType
            if powerType == 'life' then
                self.health = math.min(3, self.health + 1)
            elseif powerType == 'points' then
                self.score = self.score + 400
            elseif powerType == 'balls' then
                -- spawn two extra balls
                -- assume there is always at least one ball, copy its skin
                local newBallSkin = self.balls[1].skin
                for i = 1, 2 do
                    local newBall = Ball(newBallSkin)
                    newBall = deepcopy(self.balls[1])

                    -- shift the balls slightly
                    newBall.dx = newBall.dx + math.random(-10, 10)
                    newBall.dy = newBall.dy + math.random(-10, 2)
                    self.balls[#self.balls + i] = newBall
                end
            elseif powerType == 'ballSpeed' then
                -- increase speed for a few seconds
                for k, ball in pairs(self.balls) do
                    ball.superSpeed = true
                end
            elseif powerType == 'key' then
                gSounds['powerup']:stop()
                gSounds['unlocking']:play()

                -- give points for each lock brick
                -- convert lock bricks to normal bricks
                for _, brick in pairs(self.bricks) do
                    if brick.locked then
                        brick.locked = false
                        self.score = self.score + 150
                    end
                end
            end
        else
            table.insert(currentPowerups, powerup)
        end
    end
    self.powerups = currentPowerups

    local keyInPlay = false
    for k, powerup in pairs(self.powerups) do
        if powerup.powerupType == 'key' then
            keyInPlay = true
            break
        end
    end

    for k, brick in pairs(self.bricks) do
        -- for rendering particle systems
        brick:update(dt)

        -- check if there are any locked bricks still
        if brick.locked and not keyInPlay then
            self.keyAllowed = true
        end
    end

    if love.keyboard.wasPressed('escape') then
        love.event.quit()
    end
end

function PlayState:render()
    -- render powerups
    for k, powerup in pairs(self.powerups) do
        powerup:render()
    end

    -- render bricks
    for k, brick in pairs(self.bricks) do
        brick:render()
    end

    -- render all particle systems
    for k, brick in pairs(self.bricks) do
        brick:renderParticles()
    end

    self.paddle:render()
    for k, ball in pairs(self.balls) do
        ball:render()
    end

    renderScore(self.score)
    renderHealth(self.health)

    -- pause text, if paused
    if self.paused then
        love.graphics.setFont(gFonts['large'])
        love.graphics.printf("PAUSED", 0, VIRTUAL_HEIGHT / 2 - 16, VIRTUAL_WIDTH, 'center')
    end
end

function PlayState:checkVictory()
    for k, brick in pairs(self.bricks) do
        if brick.inPlay then
            return false
        end 
    end

    return true
end