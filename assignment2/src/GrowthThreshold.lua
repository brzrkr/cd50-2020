GrowthThreshold = Class{}

function GrowthThreshold:init()
    self.baseThreshold = 5000
    self.increment = 2500
    -- Level can go down to zero in worst cases, to let the user
    -- recover more easily if it's going really badly
    self.minLevel = 0
    self.maxLevel = 3
    self:reset()
end

function GrowthThreshold:reset()
    self.thresholdLevel = 1
end

function GrowthThreshold:getValue()
    return self.baseThreshold + self.thresholdLevel * self.increment
end

function GrowthThreshold:increase()
    self.thresholdLevel = math.min(self.maxLevel, self.thresholdLevel + 1)
end

function GrowthThreshold:decrease()
    self.thresholdLevel = math.max(self.minLevel, self.thresholdLevel - 1)
end