Powerup = Class{}

-- Powerup names mapped to their index in gFrame['powerups']
-- missing numbers are unused textures
-- The powerup type matches the index of its texture
-- a number from 1 to 10
POWERUPSKINS = {
    ['life'] = 3,
    ['balls'] = 7,
    ['points'] = 8,
    ['ballSpeed'] = 9,
    ['key'] = 10
}

function Powerup:init(powerupType)
    self.x = VIRTUAL_WIDTH / 2 - 16
    self.y = VIRTUAL_HEIGHT / 2 - 16
    self.dy = 0
    self.width = 16
    self.height = 16
    self.inPlay = false

    if not powerupType or powerupType == 'random' then
        -- If a random powerup is chosen, weigh the possible types
        -- 'key' type is special so it cannot be picked randomly
        local rand = math.random(100)
        if rand <= 10 then
            self.powerupType = 'life'
        elseif rand > 10 and rand <= 30 then
            self.powerupType = 'balls'
        elseif rand > 30 and rand <= 60 then
            self.powerupType = 'ballSpeed'
        else
            self.powerupType = 'points'
        end
    else
        self.powerupType = powerupType
    end
end

-- Spawn the powerup at a specific position and give it a downward speed
function Powerup:spawn(x, y)
    self.inPlay = true
    self.x = x
    self.y = y
    self.dy = 30 + math.random(30)
end

function Powerup:update(dt)
    if self.inPlay then
        self.y = self.y + self.dy * dt

        if self.y >= VIRTUAL_HEIGHT then
            self.inPlay = false
        end
    end
end

function Powerup:collides(target)
    if self.x > target.x + target.width or target.x > self.x + self.width then
        return false
    end

    if self.y > target.y + target.height or target.y > self.y + self.height then
        return false
    end 

    return true
end

function Powerup:render()
    if self.inPlay then
        love.graphics.draw(gTextures['main'],
            gFrames['powerups'][POWERUPSKINS[self.powerupType]],
            self.x, self.y)
    end
end