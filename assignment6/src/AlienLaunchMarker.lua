--[[
    GD50
    Angry Birds

    Author: Colton Ogden
    cogden@cs50.harvard.edu
]]

AlienLaunchMarker = Class{}

function AlienLaunchMarker:init(world)
    self.world = world

    impactOccurred = false

    -- starting coordinates for launcher used to calculate launch vector
    self.baseX = 90
    self.baseY = VIRTUAL_HEIGHT - 100

    -- shifted coordinates when clicking and dragging launch alien
    self.shiftedX = self.baseX
    self.shiftedY = self.baseY

    -- rotation for the trajectory arrow
    self.rotation = 0

    -- whether our arrow is showing where we're aiming
    self.aiming = false

    -- whether we launched the alien and should stop rendering the preview
    self.launched = false

    -- our aliens we will eventually spawn
    self.aliens = {}
end

function AlienLaunchMarker:addAlien(x, y, dX, dY)
    -- spawn new alien in the world, passing in user data of player
    local alien = Alien(self.world, 'round', x, y, 'Player')

    -- apply the difference between current X,Y and base X,Y as launch vector impulse
    alien.body:setLinearVelocity(dX, dY)

    -- make the alien pretty bouncy
    alien.fixture:setRestitution(0.4)
    alien.body:setAngularDamping(1)

    table.insert(self.aliens, alien)
end

function AlienLaunchMarker:update(dt)
    
    -- perform everything here as long as we haven't launched yet
    if self.launched then

        if love.keyboard.wasPressed('space')
        and not impactOccurred
        and #self.aliens == 1 then

            local alien = self.aliens[1].body

            local alienX, alienY = alien:getPosition()
            local alienDX, alienDY = alien:getLinearVelocity()

            local xOffset1 = math.random(-15, 15)
            local xOffset2 = math.random(-15, 15)
            local dXOffset1 = math.random(0, 20)
            local dXOffset2 = math.random(0, 20)
            local dYOffset1 = math.random(0, 15)
            local dYOffset2 = math.random(0, 15)

            self:addAlien(alienX + xOffset1, alienY - 45, alienDX + dXOffset1, alienDY + dYOffset1)

            -- if the main alien is too close to the ground, spawn the additional one above instead of below
            local lowerAlienYModifier = 45
            if alienY > VIRTUAL_HEIGHT - 90 then
                lowerAlienYModifier = lowerAlienYModifier * -2
            end

            self:addAlien(alienX + xOffset2, alienY + lowerAlienYModifier, alienDX - dXOffset2, alienDY - dYOffset2)
        end
    else
        -- grab mouse coordinates
        local x, y = push:toGame(love.mouse.getPosition())
        
        -- if we click the mouse and haven't launched, show arrow preview
        if love.mouse.wasPressed(1) and not self.launched then
            self.aiming = true

        -- if we release the mouse, launch an Alien
        elseif love.mouse.wasReleased(1) and self.aiming then
            self.launched = true

            self:addAlien(
                self.shiftedX,
                self.shiftedY,
                (self.baseX - self.shiftedX) * 10,
                (self.baseY - self.shiftedY) * 10
            )

            -- we're no longer aiming
            self.aiming = false

        -- re-render trajectory
        elseif self.aiming then
            self.rotation = self.baseY - self.shiftedY * 0.9
            self.shiftedX = math.min(self.baseX + 30, math.max(x, self.baseX - 30))
            self.shiftedY = math.min(self.baseY + 30, math.max(y, self.baseY - 30))
        end
    end
end

function AlienLaunchMarker:render()
    if not self.launched then
        
        -- render base alien, non physics based
        love.graphics.draw(gTextures['aliens'], gFrames['aliens'][9], 
            self.shiftedX - 17.5, self.shiftedY - 17.5)

        if self.aiming then
            
            -- render arrow if we're aiming, with transparency based on slingshot distance
            local impulseX = (self.baseX - self.shiftedX) * 10
            local impulseY = (self.baseY - self.shiftedY) * 10

            -- draw 6 circles simulating trajectory of estimated impulse
            local trajX, trajY = self.shiftedX, self.shiftedY
            local gravX, gravY = self.world:getGravity()

            -- http://www.iforce2d.net/b2dtut/projected-trajectory
            for i = 1, 90 do
                
                -- magenta color that starts off slightly transparent
                love.graphics.setColor(255, 80, 255, (255 / 12) * i)
                
                -- trajectory X and Y for this iteration of the simulation
                trajX = self.shiftedX + i * 1/60 * impulseX
                trajY = self.shiftedY + i * 1/60 * impulseY + 0.5 * (i * i + i) * gravY * 1/60 * 1/60

                -- render every fifth calculation as a circle
                if i % 5 == 0 then
                    love.graphics.circle('fill', trajX, trajY, 3)
                end
            end
        end
        
        love.graphics.setColor(255, 255, 255, 255)
    else
        for k, alien in pairs(self.aliens) do
            alien:render()
        end
    end
end