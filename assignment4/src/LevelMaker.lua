--[[
    GD50
    Super Mario Bros. Remake

    -- LevelMaker Class --

    Author: Colton Ogden
    cogden@cs50.harvard.edu
]]

LevelMaker = Class{}

function LevelMaker.generate(width, height)
    local tiles = {}
    local entities = {}
    local objects = {}

    local tileID = TILE_ID_GROUND

    -- spawn the key at a random x between 1/4th and the latter 4th of the level
    local sectionOfLevel = math.floor(width / 4)
    local keyLocation = math.random(sectionOfLevel, sectionOfLevel * 3)
    -- pick a specific variant of the key's looks, which will match the lock's
    local keyVariant = math.random(4)

    -- spawn the lock near the end of the level
    local lockLocation = width - math.random(4, 12)
    local lock = nil
    
    -- whether we should draw our tiles with toppers
    local topper = true
    local tileset = math.random(20)
    local topperset = math.random(20)

    -- insert blank tables into tiles for later access
    for x = 1, height do
        table.insert(tiles, {})
    end

    -- column by column generation instead of row; sometimes better for platformers
    for x = 1, width do
        local tileID = TILE_ID_EMPTY
        
        -- lay out the empty space
        for y = 1, 6 do
            table.insert(tiles[y],
                Tile(x, y, tileID, nil, tileset, topperset))
        end

        -- chance to just be emptiness
        -- skip for the first 3 tiles to ensure there is a stable initial platform for the player
        if x > 3 == 1
        -- ensure the key spawns where there is land, giving one guaranteed ground tile before and after
        and x > keyLocation + 1 and x < keyLocation - 1
        -- also ensure the last four tiles of the level don't have a chasm
        and x < width - 4
        and math.random(7) then
            for y = 7, height do
                table.insert(tiles[y],
                    Tile(x, y, tileID, nil, tileset, topperset))
            end
        else
            tileID = TILE_ID_GROUND

            local blockHeight = 4

            for y = 7, height do
                table.insert(tiles[y],
                    Tile(x, y, tileID, y == 7 and topper or nil, tileset, topperset))
            end

            -- chance to generate a pillar
            -- ensure no pillars at the end of the level
            if x < width - 4
            and math.random(8) == 1 then
                blockHeight = 2
                
                -- chance to generate bush on pillar
                if math.random(8) == 1 then
                    table.insert(objects,
                        GameObject {
                            texture = 'bushes',
                            x = (x - 1) * TILE_SIZE,
                            y = (4 - 1) * TILE_SIZE,
                            width = 16,
                            height = 16,
                            
                            -- select random frame from bush_ids whitelist, then random row for variance
                            frame = BUSH_IDS[math.random(#BUSH_IDS)] + (math.random(4) - 1) * 7
                        }
                    )
                end
                
                -- pillar tiles
                tiles[5][x] = Tile(x, 5, tileID, topper, tileset, topperset)
                tiles[6][x] = Tile(x, 6, tileID, nil, tileset, topperset)
                tiles[7][x].topper = nil
            
            -- chance to generate bushes
            elseif math.random(8) == 1 then
                table.insert(objects,
                    GameObject {
                        texture = 'bushes',
                        x = (x - 1) * TILE_SIZE,
                        y = (6 - 1) * TILE_SIZE,
                        width = 16,
                        height = 16,
                        frame = BUSH_IDS[math.random(#BUSH_IDS)] + (math.random(4) - 1) * 7,
                        collidable = false
                    }
                )
            end

            -- if a pillar was spawned, ensure stuff spawns on top of it
            local groundOffset = blockHeight == 2 and 2 or 0

            -- prevent blocks at the very start or end of a level or around the key
            -- spawn key
            if x == keyLocation then
                table.insert(objects,
                    GameObject {
                        texture = 'keys-locks',
                        x = (x - 1) * TILE_SIZE,
                        -- may spawn at a random height
                        y = (6 - math.random(0, 2) - 1 - groundOffset) * TILE_SIZE,
                        width = 16,
                        height = 16,
                        -- randomise variant
                        frame = keyVariant,
                        solid = false,
                        consumable = true,
                        onConsume = function(obj)
                            -- unlock lock
                            gSounds['powerup-reveal']:play()
                            lock.consumable = true
                        end
                    }
                )

            -- spawn lock somewhere towards the end
            elseif x == lockLocation then
                lock = GameObject {
                    texture = 'keys-locks',
                    x = (x - 1) * TILE_SIZE,
                    y = (6 - 1 - groundOffset) * TILE_SIZE,
                    width = 16,
                    height = 16,
                    -- match the key's variation
                    frame = keyVariant + 4,
                    solid = false,
                    consumable = false,
                    onConsume = function(obj)
                        gSounds['unlock']:play()
                        -- disappear and spawn flagpole at level end
                        obj = nil

                        -- pole top
                        table.insert(objects, GameObject {
                            texture = 'flags',
                            x = (width - 2) * TILE_SIZE,
                            y = (4 - 1) * TILE_SIZE,
                            width = 16,
                            height = 16,

                            -- skip the first two variants
                            -- (they don't have a corresponding flag and don't agree with the keys/locks)
                            frame = 2 + keyVariant,
                            solid = false
                        })

                        -- pole mid
                        table.insert(objects, GameObject {
                            texture = 'flags',
                            x = (width - 2) * TILE_SIZE,
                            y = (5 - 1) * TILE_SIZE,
                            width = 16,
                            height = 16,
                            frame = 11 + keyVariant,
                            solid = false
                        })

                        -- pole bottom
                        table.insert(objects, GameObject {
                            texture = 'flags',
                            x = (width - 2) * TILE_SIZE,
                            y = (6 - 1) * TILE_SIZE,
                            width = 16,
                            height = 16,
                            frame = 20 + keyVariant,
                            solid = false
                        })

                        -- flag
                        table.insert(objects, GameObject {
                            texture = 'flags',
                            x = (width - 2) * TILE_SIZE + (TILE_SIZE / 2),
                            y = (4 - 1) * TILE_SIZE,
                            width = 16,
                            height = 16,
                            frame = 7 + 9 * (keyVariant - 1),
                            solid = false,
                            consumable = true,
                            onConsume = function(player)
                                gSounds['change-level']:play()
                                -- start new level
                                gStateMachine:change('play', {
                                    width = width,
                                    score = player.score
                                })
                            end
                        })
                    end
                }

                table.insert(objects, lock)

            -- chance to spawn a block
            elseif x > 4 and x < width - 3 and math.random(10) == 1 then
                table.insert(objects,

                    -- jump block
                    GameObject {
                        texture = 'jump-blocks',
                        x = (x - 1) * TILE_SIZE,
                        y = (blockHeight - 1) * TILE_SIZE,
                        width = 16,
                        height = 16,

                        -- make it a random variant
                        frame = math.random(#JUMP_BLOCKS),
                        collidable = true,
                        hit = false,
                        solid = true,

                        -- collision function takes itself
                        onCollide = function(obj)

                            -- spawn a gem if we haven't already hit the block
                            if not obj.hit then

                                -- chance to spawn gem, not guaranteed
                                if math.random(5) == 1 then

                                    -- maintain reference so we can set it to nil
                                    local gem = GameObject {
                                        texture = 'gems',
                                        x = (x - 1) * TILE_SIZE,
                                        y = (blockHeight - 1) * TILE_SIZE - 4,
                                        width = 16,
                                        height = 16,
                                        frame = math.random(#GEMS),
                                        collidable = true,
                                        consumable = true,
                                        solid = false,

                                        -- gem has its own function to add to the player's score
                                        onConsume = function(player, object)
                                            gSounds['pickup']:play()
                                            player.score = player.score + 100
                                        end
                                    }
                                    
                                    -- make the gem move up from the block and play a sound
                                    Timer.tween(0.1, {
                                        [gem] = {y = (blockHeight - 2) * TILE_SIZE}
                                    })
                                    gSounds['powerup-reveal']:play()

                                    table.insert(objects, gem)
                                end

                                obj.hit = true
                            end

                            gSounds['empty-block']:play()
                        end
                    }
                )
            end
        end
    end

    local map = TileMap(width, height)
    map.tiles = tiles
    
    return GameLevel(entities, objects, map)
end