--[[
    GD50
    Match-3 Remake

    -- Tile Class --

    Author: Colton Ogden
    cogden@cs50.harvard.edu

    The individual tiles that make up our game board. Each Tile can have a
    color and a variety, with the varietes adding extra points to the matches.
]]

Tile = Class{}

function Tile:init(x, y, color, variety, isShiny)
    
    -- board positions
    self.gridX = x
    self.gridY = y

    -- coordinate positions
    self.x = (self.gridX - 1) * 32
    self.y = (self.gridY - 1) * 32

    -- tile appearance/points
    self.color = color
    self.variety = variety
    self.isShiny = isShiny or false

    if self.isShiny then
        self.pSystem = love.graphics.newParticleSystem(gTextures['shiny'], 50)
        self.pSystem:setParticleLifetime(3)
        self.pSystem:setSizeVariation(1)
        self.pSystem:setLinearAcceleration(-5, -5, 5, 5) -- emit in random directions
        self.pSystem:setColors(255, 255, 255, 120, 255, 255, 255, 0) -- fade to full transparency
        self.pSystem:setSpin(6, 12) -- spin 1 to 2 times per second
        self.pSystem:setAreaSpread('uniform', 14, 14)
    end
end

function Tile:update(dt)
    if self.isShiny then
        self.pSystem:emit(1)
        self.pSystem:update(dt)
    end
end

function Tile:render(x, y)
    
    -- draw shadow
    love.graphics.setColor(34, 32, 52, 255)
    love.graphics.draw(gTextures['main'], gFrames['tiles'][self.color][self.variety],
        self.x + x + 2, self.y + y + 2)

    -- draw tile itself
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.draw(gTextures['main'], gFrames['tiles'][self.color][self.variety],
        self.x + x, self.y + y)
end

function Tile:renderPSystem(x, y)
    -- if shiny, draw particle effects
    if self.isShiny then
        love.graphics.draw(self.pSystem, self.x + x + 16, self.y + y + 16)
    end
end