--[[
    GD50
    Match-3 Remake

    -- Board Class --

    Author: Colton Ogden
    cogden@cs50.harvard.edu

    The Board is our arrangement of Tiles with which we must try to find matching
    sets of three horizontally or vertically.
]]


-- limit the colours that can appear to make the game
-- more accessible and reduce the chance of deadlocks.
AVAILABLE_COLOURS = {
    1, -- sand
    4, -- green
    6, -- blue
    8, -- purple
    11, -- pink
    12, -- red
    15, -- orange
    16, -- lightgrey
    13, -- darkred
    14 -- brown
}


Board = Class{}

function Board:init(x, y, level)
    self.x = x
    self.y = y
    self.matches = {}
    self.level = level

    self:initializeTiles()
end

function Board:initializeTiles()
    self.tiles = {}

    for tileY = 1, 8 do
        
        -- empty table that will serve as a new row
        table.insert(self.tiles, {})

        for tileX = 1, 8 do
            
            -- create a new tile at X,Y with a random color and variety
            table.insert(self.tiles[tileY], self:generateTile(tileX, tileY))
        end
    end

    while self:calculateMatches() or self:isDeadlocked() do
        
        -- recursively initialize if matches were returned
        -- or if the board is deadlocked
        -- so we always have a playable, matchless board on start
        self:initializeTiles()
    end
end

--[[
    Generate a tile according to the current level.
]]
function Board:generateTile(x, y)
    -- generate variety based on level
    -- level 1 has only plain variety
    -- other levels have 5% chance per level per tile of being non-1
    local variety = 1
    local chanceOfVariety = (self.level - 1) * 5
    if math.random(100) <= chanceOfVariety then

        -- varieties are distributed with reducing chances
        local varChance = math.random(10)

        if varChance <= 3 then
            variety = 2
        elseif varChance <= 6 then
            variety = 3
        elseif varChance <= 8 then
            variety = 4
        elseif varChance <= 9 then
            variety = 5
        else
            variety = 6
        end
    end

    -- chance of shiny tiles is 3% per tile plus .5 per level
    local isShiny = false
    if math.random(1000) <= 30 + (0.5 * self.level) then
        isShiny = true
    end

    -- generate colour based on level
    -- each level after the first adds 1 colour up to
    -- the number of available colours
    local colours = 7
    colours = math.min(#AVAILABLE_COLOURS, colours + (self.level - 1))

    return Tile(x, y, AVAILABLE_COLOURS[math.random(colours)], variety, isShiny)
end

--[[
    Return true if there is no possible match on the board.
]]
function Board:isDeadlocked()
    for thisRow, rowContents in pairs(self.tiles) do
        for thisColumn, tile in pairs(rowContents) do
            local thisColour = tile.color
            
            -- check linear matches
            -- horizontally
            local nextColour = rowContents[thisColumn+1]
            nextColour = nextColour and nextColour.color or nil
            local nextThirdColour = rowContents[thisColumn+3]
            nextThirdColour = nextThirdColour and nextThirdColour.color or nil

            if thisColour == nextColour
            and thisColour == nextThirdColour then
                return false
            end

            -- vertically
            local nextColourSouth = self.tiles[thisRow+1]
            nextColourSouth = nextColourSouth and nextColourSouth[thisColumn] or nil
            nextColourSouth = nextColourSouth and nextColourSouth.color or nil
            local nextThirdColourSouth = self.tiles[thisRow+3]
            nextThirdColourSouth = nextThirdColourSouth and nextThirdColourSouth[thisColumn] or nil
            nextThirdColourSouth = nextThirdColourSouth and nextThirdColourSouth.color or nil

            if thisColour == nextColourSouth
            and thisColour == nextThirdColourSouth then
                return false
            end

            -- check triangular matches
            -- horizontally
            local nextSecondColour = rowContents[thisColumn+2]
            nextSecondColour = nextSecondColour and nextSecondColour.color or nil
            local colourSouthEast = self.tiles[thisRow+1]
            colourSouthEast = colourSouthEast and colourSouthEast[thisColumn+1] or nil
            colourSouthEast = colourSouthEast and colourSouthEast.color or nil
            local colourNorthEast = self.tiles[thisRow-1]
            colourNorthEast = colourNorthEast and colourNorthEast[thisColumn+1] or nil
            colourNorthEast = colourNorthEast and colourNorthEast.color or nil

            if thisColour == nextSecondColour then
                if thisColour == colourSouthEast
                or thisColour == colourNorthEast then
                    return false
                end
            end

            -- vertically
            local nextSecondColourSouth = self.tiles[thisRow+2]
            nextSecondColourSouth = nextSecondColourSouth and nextSecondColourSouth[thisColumn] or nil
            nextSecondColourSouth = nextSecondColourSouth and nextSecondColourSouth.color or nil
            local colourSouthWest = self.tiles[thisRow+1]
            colourSouthWest = colourSouthWest and colourSouthWest[thisColumn-1] or nil
            colourSouthWest = colourSouthWest and colourSouthWest.color or nil

            if thisColour == nextSecondColourSouth then
                if thisColour == colourSouthEast
                or thisColour == colourSouthWest then
                    return false
                end
            end
        end
    end

    return true
end

--[[
    Goes left to right, top to bottom in the board, calculating matches by counting consecutive
    tiles of the same color. Doesn't need to check the last tile in every row or column if the 
    last two haven't been a match.
]]
function Board:calculateMatches()
    local matches = {}

    -- how many of the same color blocks in a row we've found
    local matchNum = 1

    -- horizontal matches first
    for y = 1, 8 do
        local colorToMatch = self.tiles[y][1].color

        matchNum = 1
        
        -- every horizontal tile
        for x = 2, 8 do
            
            -- if this is the same color as the one we're trying to match...
            if self.tiles[y][x].color == colorToMatch then
                matchNum = matchNum + 1
            else
                
                -- set this as the new color we want to watch for
                colorToMatch = self.tiles[y][x].color

                -- if we have a match of 3 or more up to now, add it to our matches table
                if matchNum >= 3 then
                    local match = {}

                    -- go backwards from here by matchNum
                    for x2 = x - 1, x - matchNum, -1 do
                        
                        -- add each tile to the match that's in that match
                        table.insert(match, self.tiles[y][x2])
                    end

                    -- add this match to our total matches table
                    table.insert(matches, match)
                end

                matchNum = 1

                -- don't need to check last two if they won't be in a match
                if x >= 7 then
                    break
                end
            end
        end

        -- account for the last row ending with a match
        if matchNum >= 3 then
            local match = {}
            
            -- go backwards from end of last row by matchNum
            for x = 8, 8 - matchNum + 1, -1 do
                table.insert(match, self.tiles[y][x])
            end

            table.insert(matches, match)
        end
    end

    -- vertical matches
    for x = 1, 8 do
        local colorToMatch = self.tiles[1][x].color

        matchNum = 1

        -- every vertical tile
        for y = 2, 8 do
            if self.tiles[y][x].color == colorToMatch then
                matchNum = matchNum + 1
            else
                colorToMatch = self.tiles[y][x].color

                if matchNum >= 3 then
                    local match = {}

                    for y2 = y - 1, y - matchNum, -1 do
                        table.insert(match, self.tiles[y2][x])
                    end

                    table.insert(matches, match)
                end

                matchNum = 1

                -- don't need to check last two if they won't be in a match
                if y >= 7 then
                    break
                end
            end
        end

        -- account for the last column ending with a match
        if matchNum >= 3 then
            local match = {}
            
            -- go backwards from end of last row by matchNum
            for y = 8, 8 - matchNum + 1, -1 do
                table.insert(match, self.tiles[y][x])
            end

            table.insert(matches, match)
        end
    end

    -- store matches for later reference
    self.matches = matches

    -- return matches table if > 0, else just return false
    return #self.matches > 0 and self.matches or false
end

--[[
    Remove the matches from the Board by just setting the Tile slots within
    them to nil, then setting self.matches to nil.
]]
function Board:removeMatches()
    for k, match in pairs(self.matches) do
        for k, tile in pairs(match) do
            -- if a tile in a match is shiny, remove its entire row
            if tile.isShiny then
                for i = 1, 8 do
                    self.tiles[tile.gridY][i] = nil
                end
            else
                self.tiles[tile.gridY][tile.gridX] = nil
            end
        end
    end

    self.matches = nil
end

--[[
    Shifts down all of the tiles that now have spaces below them, then returns a table that
    contains tweening information for these new tiles.
]]
function Board:getFallingTiles()
    -- tween table, with tiles as keys and their x and y as the to values
    local tweens = {}

    -- for each column, go up tile by tile till we hit a space
    for x = 1, 8 do
        local space = false
        local spaceY = 0

        local y = 8
        while y >= 1 do
            
            -- if our last tile was a space...
            local tile = self.tiles[y][x]
            
            if space then
                
                -- if the current tile is *not* a space, bring this down to the lowest space
                if tile then
                    
                    -- put the tile in the correct spot in the board and fix its grid positions
                    self.tiles[spaceY][x] = tile
                    tile.gridY = spaceY

                    -- set its prior position to nil
                    self.tiles[y][x] = nil

                    -- tween the Y position to 32 x its grid position
                    tweens[tile] = {
                        y = (tile.gridY - 1) * 32
                    }

                    -- set Y to spaceY so we start back from here again
                    space = false
                    y = spaceY

                    -- set this back to 0 so we know we don't have an active space
                    spaceY = 0
                end
            elseif tile == nil then
                space = true
                
                -- if we haven't assigned a space yet, set this to it
                if spaceY == 0 then
                    spaceY = y
                end
            end

            y = y - 1
        end
    end

    -- create replacement tiles at the top of the screen
    for x = 1, 8 do
        for y = 8, 1, -1 do
            local tile = self.tiles[y][x]

            -- if the tile is nil, we need to add a new one
            if not tile then

                -- new tile with random color and variety
                local tile = self:generateTile(x, y)
                tile.y = -32
                self.tiles[y][x] = tile

                -- create a new tween to return for this tile to fall down
                tweens[tile] = {
                    y = (tile.gridY - 1) * 32
                }
            end
        end
    end

    return tweens
end

function Board:update(dt)
    for k, row in pairs(self.tiles) do
        for k, tile in pairs(row) do
            tile:update(dt)
        end
    end
end

function Board:render()
    for y = 1, #self.tiles do
        for x = 1, #self.tiles[1] do
            self.tiles[y][x]:render(self.x, self.y)
        end
    end

    -- trigger particle systems afterwards to avoid
    -- particles from being drawn under tiles
    for y = 1, #self.tiles do
        for x = 1, #self.tiles[1] do
            self.tiles[y][x]:renderPSystem(self.x, self.y)
        end
    end
end