--[[
    GD50
    Legend of Zelda

    Author: Colton Ogden
    cogden@cs50.harvard.edu
]]

PlayerLiftIdleState = Class{__includes = EntityIdleState}

function PlayerLiftIdleState:init(player, dungeon)
    self.entity = player
    self.dungeon = dungeon

    -- render offset for spaced character sprite
    self.entity.offsetY = 5
    self.entity.offsetX = 0
end

function PlayerLiftIdleState:update(dt)
    EntityIdleState.update(self, dt)
end

function PlayerLiftIdleState:update(dt)
    if love.keyboard.isDown('left') or love.keyboard.isDown('right') or
       love.keyboard.isDown('up') or love.keyboard.isDown('down') then
        self.entity:changeState('lift-walk')
    end

    if love.keyboard.wasPressed('space') then
        local pot = GAME_OBJECT_DEFS['pot-projectile']
        pot['direction'] = self.entity.direction

        table.insert(self.dungeon.currentRoom.objects, GameObject(
            pot,
            math.floor(self.entity.x - self.entity.offsetX),
            math.floor(self.entity.y - self.entity.offsetY)
        ))
        self.entity:changeState('idle')
    end

    if self.entity.direction == 'left' then
        self.entity:changeAnimation('lift-idle-left')
    elseif self.entity.direction == 'right' then
        self.entity:changeAnimation('lift-idle-right')
    elseif self.entity.direction == 'down' then
        self.entity:changeAnimation('lift-idle-down')
    else
        self.entity:changeAnimation('lift-idle-up')
    end
end

function PlayerLiftIdleState:render()
    EntityIdleState.render(self)

    love.graphics.draw(
        gTextures['tiles'],
        gFrames['tiles'][110],
        math.floor(self.entity.x - self.entity.offsetX),
        math.floor(self.entity.y - self.entity.offsetY)
    )
end