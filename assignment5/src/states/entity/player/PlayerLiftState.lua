--[[
--[[
    GD50
    Legend of Zelda

    Author: Colton Ogden
    cogden@cs50.harvard.edu
]]

PlayerLiftState = Class{__includes = EntityIdleState}

function PlayerLiftState:enter(params)
    -- render offset for spaced character sprite
    self.entity.offsetY = 5
    self.entity.offsetX = 0

    local direction = self.entity.direction

    local xOffset = 0
    local yOffset = 0

    if direction == 'left' then
        xOffset = -16
    elseif direction == 'right' then
        xOffset = 16
    elseif direction == 'down' then
        yOffset = 16
    else
        yOffset = -16
    end

    self.potX = math.floor(self.entity.x - self.entity.offsetX + xOffset)
    self.potY = math.floor(self.entity.y - self.entity.offsetY + yOffset)
end

function PlayerLiftState:update(dt)
    self.entity:changeAnimation('lift-' .. self.entity.direction)

    local anim = self.entity.currentAnimation
    if anim.currentFrame == #anim.frames then
        self.entity:changeState('lift-idle')
    end

    Timer.tween(0.2, {
        [self] = {
            potX = math.floor(self.entity.x - self.entity.offsetX),
            potY = math.floor(self.entity.y - self.entity.offsetY)
        }
    })
end

function PlayerLiftState:render()
    PlayerIdleState.render(self)

    love.graphics.draw(
        gTextures['tiles'],
        gFrames['tiles'][110],
        self.potX,
        self.potY
    )
end