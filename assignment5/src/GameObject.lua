--[[
    GD50
    Legend of Zelda

    Author: Colton Ogden
    cogden@cs50.harvard.edu
]]

GameObject = Class{}

function GameObject:init(def, x, y)
    -- string identifying this object type
    self.type = def.type

    self.texture = def.texture
    self.frame = def.frame or 1

    -- whether it acts as an obstacle or not
    self.solid = def.solid

    self.defaultState = def.defaultState
    self.state = self.defaultState
    self.states = def.states
    self.collectible = def.collectible or false
    self.liftable = def.liftable or false
    self.projectile = def.projectile or false

    -- dimensions
    self.x = x
    self.y = y
    self.width = def.width
    self.height = def.height

    if self.projectile then
        self.distanceTravelled = 0

        local speed = PLAYER_WALK_SPEED * 3
        if def.direction == 'left' then
            self.dx = -speed
            self.dy = 0
        elseif def.direction == 'right' then
            self.dx = speed
            self.dy = 0
        elseif def.direction == 'up' then
            self.dx = 0
            self.dy = -speed
        else
            self.dx = 0
            self.dy = speed
        end
    end

    -- default empty collision callback
    self.onCollide = def.onCollide or function() end
end

function GameObject:update(dt)
    if not self.projectile then return end

    local xVelocity = (self.dx * dt)
    local yVelocity = (self.dy * dt)

    self.x = self.x + xVelocity
    self.y = self.y + yVelocity
    
    self.distanceTravelled = self.distanceTravelled + math.abs(xVelocity + yVelocity)
end

function GameObject:render(adjacentOffsetX, adjacentOffsetY)
    love.graphics.draw(gTextures[self.texture], gFrames[self.texture][self.states[self.state].frame or self.frame],
        self.x + adjacentOffsetX, self.y + adjacentOffsetY)
end