--[[
    GD50
    Legend of Zelda

    Author: Colton Ogden
    cogden@cs50.harvard.edu
]]

GAME_OBJECT_DEFS = {
    ['switch'] = {
        type = 'switch',
        texture = 'switches',
        frame = 2,
        width = 16,
        height = 16,
        solid = false,
        defaultState = 'unpressed',
        states = {
            ['unpressed'] = {
                frame = 2
            },
            ['pressed'] = {
                frame = 1
            }
        }
    },
    ['pot'] = {
        type = 'pot',
        texture = 'tiles',
        frame = 110,
        width = 16,
        height = 16,
        solid = true,
        liftable = true,
        defaultState = 'default',
        states = {
            ['default'] = {}
        }
    },
    ['pot-projectile'] = {
        type = 'pot',
        texture = 'tiles',
        frame = 111,
        width = 16,
        height = 16,
        solid = false,
        projectile = true,
        defaultState = 'default',
        states = {
            ['default'] = {}
        },
        onCollide = function(obj, entity)
            gSounds['crash']:play()
            
            if entity then
                entity.health = math.max(0, entity.health - 1)
            end
        end
    },
    ['heart'] = {
        type = 'heart',
        texture = 'hearts',
        frame = 5,
        width = 16,
        height = 16,
        solid = false,

        -- collectible objects are removed once they collide with the player
        collectible = true,
        defaultState = 'default',
        states = {
            ['default'] = {}
        },
        onCollide = function(obj, player)
            gSounds['recover']:play()
            player.health = math.min(6, player.health + 2)
        end
    }
}